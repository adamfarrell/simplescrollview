//
//  ViewController.m
//  SimpleScrollview
//
//  Created by Adam Farrell on 5/16/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    float height = self.view.frame.size.height;
    float width = self.view.frame.size.width;
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    [scrollView setContentSize:CGSizeMake(width * 5, height)];
    [scrollView setPagingEnabled:YES];
    [self.view addSubview:scrollView];
    
    NSArray* titles = @[@"Slow Cooker Chicken and Dumplings", @"Chicken Pot Pie IX", @"Zesty Slowcooker Chicken Barbecue", @"Sweet, Sticky and Spicy Chicken", @"Baked Teriyaki Chicken"];
    
    NSArray* images = @[@"chkndump.tiff", @"chknpie.tiff", @"chknbbq.tiff", @"chknspicy.tiff", @"chknteriyaki.tiff"];
    
    NSArray* textBlocks = @[@"Ingredients\nOriginal recipe makes 8 servings\n\n4 skinless, boneless chicken breast halves\n2 tablespoons butter\n2 (10.75 ounce) cans condensed cream of chicken soup\n1 onion, finely diced\n2 (10 ounce) packages refrigerated biscuit dough, torn into pieces\n\n\nDirections\n\n1. Place the chicken, butter, soup, and onion in a slow cooker, and fill with enough water to cover.\n2. Cover, and cook for 5 to 6 hours on High. About 30 minutes before serving, place the torn biscuit dough in the slow cooker. Cook until the dough is no longer raw in the center.", @"Ingredients\nOriginal recipe makes 1 9-inch pie\n\n1 pound skinless, boneless chicken breast halves - cubed\n1 cup sliced carrots\n1 cup frozen green peas\n1/2 cup sliced celery\n1/3 cup butter\n1/3 cup chopped onion\n1/3 cup all-purpose flour\n1/2 teaspoon salt\n1/4 teaspoon black pepper\n1/4 teaspoon celery seed\n1 3/4 cups chicken broth\n2/3 cup milk\n2 (9 inch) unbaked pie crusts\n\n\nDirections\n\n1. Preheat oven to 425 degrees F (220 degrees C.)\n2. In a saucepan, combine chicken, carrots, peas, and celery. Add water to cover and boil for 15 minutes. Remove from heat, drain and set aside.\n3. In the saucepan over medium heat, cook onions in butter until soft and translucent. Stir in flour, salt, pepper, and celery seed. Slowly stir in chicken broth and milk. Simmer over medium-low heat until thick. Remove from heat and set aside.\n4. Place the chicken mixture in bottom pie crust. Pour hot liquid mixture over. Cover with top crust, seal edges, and cut away excess dough. Make several small slits in the top to allow steam to escape.\n5. Bake in the preheated oven for 30 to 35 minutes, or until pastry is golden brown and filling is bubbly. Cool for 10 minutes before serving.", @"Ingredients\nOriginal recipe makes 6 servings\n\n6 frozen skinless, boneless chicken breast halves\n1 (12 ounce) bottle barbeque sauce\n1/2 cup Italian salad dressing\n1/4 cup brown sugar\n2 tablespoons Worcestershire sauce\n\n\nDirections\n\n1. Place chicken in a slow cooker. In a bowl, mix the barbecue sauce, Italian salad dressing, brown sugar, and Worcestershire sauce. Pour over the chicken.\n2. Cover, and cook 3 to 4 hours on High or 6 to 8 hours on Low.", @"Ingredients\nOriginal recipe makes 4 servings\n\n1 tablespoon brown sugar\n2 tablespoons honey\n1/4 cup soy sauce\n2 teaspoons chopped fresh ginger root\n2 teaspoons chopped garlic\n2 tablespoons hot sauce\nsalt and pepper to taste\n4 skinless, boneless chicken breast halves - cut into 1/2 inch strips\n1 tablespoon vegetable oil\n\n\nDirections\n\n1. Mix together brown sugar, honey, soy sauce, ginger, garlic and hot sauce in a small bowl.\nLightly salt and pepper the chicken strips.\n2. Heat oil in a large skillet over medium heat. Add chicken strips and brown on both sides, about 1 minute per side. Pour the sauce over the chicken. Simmer uncovered until the sauce thickens, 8 to 10 minutes.", @"Ingredients\nOriginal recipe makes 6 servings\n\n1 tablespoon cornstarch\n1 tablespoon cold water\n1/2 cup white sugar\n1/2 cup soy sauce\n1/4 cup cider vinegar\n1 clove garlic, minced\n1/2 teaspoon ground ginger\n1/4 teaspoon ground black pepper\n12 skinless chicken thighs\n\n\nDirections\n\n1. In a small saucepan over low heat, combine the cornstarch, cold water, sugar, soy sauce, vinegar, garlic, ginger and ground black pepper. Let simmer, stirring frequently, until sauce thickens and bubbles.\n2. Preheat oven to 425 degrees F (220 degrees C).\n3. Place chicken pieces in a lightly greased 9x13 inch baking dish. Brush chicken with the sauce. Turn pieces over, and brush again.\n4. Bake in the preheated oven for 30 minutes. Turn pieces over, and bake for another 30 minutes, until no longer pink and juices run clear. Brush with sauce every 10 minutes during cooking."];
    
    for (int i = 0; i < 5; i++) {
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(width * i, 0, width, height)];
        
        UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, width - 40, height / 4)];
        image.contentMode = UIViewContentModeScaleAspectFit;
        [image setImage:[UIImage imageNamed:[images objectAtIndex:i]]];
        [view addSubview:image];
        
        UILabel* title = [[UILabel alloc]initWithFrame:CGRectMake(20, height / 4 + 30, width - 40, 40)];
        title.text = [titles objectAtIndex:i];
        title.numberOfLines = 1;
        title.adjustsFontSizeToFitWidth = YES;
        title.textAlignment = NSTextAlignmentCenter;
        [view addSubview:title];
        
        UITextView* textView = [[UITextView alloc]initWithFrame:CGRectMake(20, height / 4 + 80, width - 40, height / 2)];
        textView.text = [textBlocks objectAtIndex:i];
        [view addSubview:textView];
        
        [scrollView addSubview:view];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
